package com.chmelar.autofuniacom;

import android.content.Intent;
import android.graphics.Typeface;
import android.renderscript.Type;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.textViewHeader)  // id komponentu v  res/layout/activity_main.xml
    TextView header;  //moje meno v triede.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this); //napojenie komponentov na premmnne
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/NYC.ttf");//nastavi font
        header.setTypeface(font);

    }


    @OnClick(R.id.btnSendFoto)void sendFoto(){
        Intent i = new Intent(this,UploadActivity.class);//poziadavka na spustenie aktivity
        if(util.isNetworkAvailable(this)) //kontrola konektivity
        startActivity(i); //vykonanie poziadavky
        else
            Toast.makeText(MainActivity.this, "No network", Toast.LENGTH_SHORT).show();
    }
//po kliiknuti na rozne komnponenty v layoute
    @OnClick(R.id.btnHome) void home(){
        util.openInBrowser(this, "http://autofunia.com/");
    }
    @OnClick(R.id.btnKontakt) void kontakt(){
        Intent i = new Intent(this,KontaktActivity.class);
        startActivity(i); //spusti aktivitu Kontakt.
    }
    @OnClick(R.id.btnLogin) void login(){
       util.openInBrowser(this, "http://autofunia.com/register/");
    }

    @OnClick(R.id.btnFacebook)
        //pri kliknuti na facebook sa otvori stranka
    void facebook() {
        util.openInBrowser(this, "https://www.facebook.com/autofunia/");
    }

    @OnClick(R.id.btnInstagram)
    void instagram() {
        util.openInBrowser(this, "https://www.instagram.com/autofunia/");
    }

    @OnClick(R.id.btnTwitter)
    void twitter() {
        util.openInBrowser(this, "https://twitter.com/AUTOFUNIA_com");
    }
}
