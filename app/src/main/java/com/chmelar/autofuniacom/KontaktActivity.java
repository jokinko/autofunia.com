package com.chmelar.autofuniacom;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KontaktActivity extends AppCompatActivity {
  //spojenie premennej a komponentu v layoute
    @Bind(R.id.headerKontakt)
    TextView header;
    @Bind(R.id.textViewPopisLoga)
    TextView popis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //onCreate sa vykresli vzdy po spusteni aktivity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontakt);
        ButterKnife.bind(this); //napojenie komponentov
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/NYC.ttf"); //nastavenie fontu
        header.setTypeface(font);
        popis.setTypeface(font);
    }

    @OnClick(R.id.btnFacebook)
        //pri kliknuti na facebook sa otvori stranka
    void facebook() {
        util.openInBrowser(this, "https://www.facebook.com/autofunia/");
    }

    @OnClick(R.id.btnInstagram)
    void instagram() {
        util.openInBrowser(this, "https://www.instagram.com/autofunia/");
    }

    @OnClick(R.id.btnTwitter)
    void kontakt() {
        util.openInBrowser(this, "https://twitter.com/AUTOFUNIA_com");
    }

    @OnClick(R.id.btnMail)
    void mail() { //spusti klienta na poslanie mailu
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"radovan@blazicek.com"});

        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
