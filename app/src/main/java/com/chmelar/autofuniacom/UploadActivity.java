package com.chmelar.autofuniacom;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class UploadActivity extends AppCompatActivity {

    //region binding
    @Bind(R.id.editTextName)
    EditText name;
    @Bind(R.id.editTextMail)
    EditText mail;
    @Bind(R.id.editTextCaptcha)
    EditText captcha;
    @Bind(R.id.editTextContent)
    EditText content;
    @Bind(R.id.imageViewImage)
    ImageView image;
    @Bind(R.id.textViewInsertPicture)
    TextView textViewInsertPicture;
    //endregion
    private static final String TAG = "chmelar"; //tag pre logovanie a chladanie chyb v logcate
    private Uri uri; //relativna adresa
    private String picPath; //adresa obrazku v pamati telefonu
    private String nonce;  //unikatny kod pre upload na autofunii

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);
        setCaptcha(); //ziskanie captcha zo stranky
        try {
            //tu spracovavam obrazok kt. dostanem z galerie.
            Intent i = getIntent();
            Bundle bundle = i.getExtras();
            Uri uri = (Uri) bundle.get(Intent.EXTRA_STREAM); //dostanem adresu obrazku z galerie
            picPath = util.getRealPathFromURI2(this, uri); //dekodujem na realnu adresu
            image.setImageBitmap(BitmapFactory.decodeFile(picPath)); //dekodujem subor na bitmapu a nastavim aby sa zobrazil
        } catch (Exception exc) {
            Log.i("chmelar", exc.toString());
        }
    }

    private void setCaptcha() {
        //poziadavka get na upload stranku
        Ion.with(this).load("GET", "http://autofunia.com/send-picture/")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        // vrati cely html kod stranky ako result
                        Document doc = Jsoup.parse(result.getResult());
                        //hladam v html kode cpatchu a usp-nonce kod
                        String captchaText = doc.getElementsByClass("usp-captcha").first().text();
                        nonce = doc.getElementsByClass("exclude").last().nextElementSibling().attr("value"); //usp-nonce
                        captcha.setHint(captchaText); //nastavim hint v textfielde podla stranky
                    }
                });
    }

    @OnClick(R.id.buttonUpload)
    void upload() {
        //paramtere pre POST
        RequestParams params = new RequestParams();
        try {
            File toUpload = new File(picPath); // obrazok
            // tu nastavim parametere pre POST podla
            // <form id="usp_form" a jeho input parametorv.
            params.put("user-submitted-image[]", toUpload, "image/" + util.getFileExt(toUpload.getName().toLowerCase())); //zistim meno suboru a jeho koncovku ..typ suboru
            params.put("user-submitted-title", name.getText().toString()); // user-submitet-name = musi byt rovnaky ako je v html form
            params.put("user-submitted-email", mail.getText().toString()); //ziskavam texty z poli
            params.put("user-submitted-content", content.getText().toString());
            params.put("user-submitted-captcha", captcha.getText().toString());
            params.put("usp-min-images", "1"); //presne dane na stranke.
            params.put("usp-max-images", "1");
         //   params.put("usp-max-images", "1");
        //    params.put("user-submitted-verify", "");
        //    params.put("user-submitted-name", "");
            params.put("user-submitted-post", "Submit Post");
            params.put("usp-nonce", nonce);

            AsyncHttpClient client = new AsyncHttpClient(); //klient na odosielanie obrazku

            if (util.isNetworkAvailable(this)) {
                Toast.makeText(UploadActivity.this, "Your picture is uploading", Toast.LENGTH_LONG).show();
                client.post(this, "http://autofunia.com/send-picture/", params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(UploadActivity.this, "Thank you for your upload", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        //server posiela fail aj ked uploadne spravne.
                        Toast.makeText(UploadActivity.this, "Thank you for your non-upload", Toast.LENGTH_LONG).show();
                    }
                });
                //uokoncim aktivitu
                finish();
            } else {
                Toast.makeText(UploadActivity.this, "No network", Toast.LENGTH_SHORT).show();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(UploadActivity.this, "file not found exp", Toast.LENGTH_SHORT).show();
            Toast.makeText(UploadActivity.this, picPath, Toast.LENGTH_SHORT).show();
        } catch (NullPointerException e) {
            Toast.makeText(UploadActivity.this, "no picture", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.imageViewImage)
    void selectImage() {
        //ked kliknem na obrazok, zvolim si jeden z galerie.
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //ak je vsetko v poriadku, a mam spravny kod na prijatie obrazka z galerie idem dalej
        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            uri = selectedImage;  //adreasa obrazka
            Log.i("file", "onActivityResult  " + selectedImage.getPath() + " uri " + selectedImage.toString());
            //magia
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            picPath = picturePath;
            image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            textViewInsertPicture.setVisibility(View.GONE);
        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

    }
}
